const Assert = require('./assert.js').Assert
const Throw = require('./assert.js').Throw

describe('Assert()', () => {
	it('a false should throw an error', () => {
		expect(() => Assert(num => false)(0, 'I am a test Error message')).toThrowError()
	})
	it('a truth should not throw an error', () => {
		expect(() => Assert(num => true)(0, 'I am a test Error message')).not.toThrowError()
	})
	it('a type error (number does not have split method) show throw an error with a custom message', () => {
		expect(() => Assert(num => num.split(':'))(0, 'I am a test Error message')).toThrowError(
			'I am a test Error message'
		)
	})
})

describe('Throw()', () => {
	it('a false should throw an error', () => {
		expect(() => Throw('I am a test Error message')).toThrowError()
	})
})
describe('Throw()', () => {
	it('a false should throw an error with the correct message', () => {
		expect(() => Throw('I am a test Error message')).toThrowError('I am a test Error message')
	})
})
