// Assert
// Assert is a help function for preconditions
// if Assert is false return an error
const Assert = fn => (v, msg) => {
	var t
	try {
		t = fn(v)
	} catch (e) {
		throw new Error(msg)
	}
	if (!t) Throw(msg)
}

const Throw = msg => {
	try {
		throw new Error(msg)
	} catch (e) {
		throw e
	}
}

module.exports = { Assert, Throw }
