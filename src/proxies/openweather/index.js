const APIAdaptor = require('../../utils/api-adaptor.js')
const Assert = require('../../utils/assert.js').Assert
const Logger = require('../../utils/logger.js').Logger

const errShouldContainQueryParam = 'we expected request to contain a query param'
const shouldContainQueryParam = obj => obj.hasOwnProperty('query')

const errShouldContainQParam = "we expected request to contain a 'q' in the query param"
const shouldContainQParam = obj => obj.hasOwnProperty('q')

const key = '708c6b9f5408a4985e64bfbad5fe940d'
const adaptor = APIAdaptor('https://api.openweathermap.org/data/2.5/forecast')
const query = obj => '?q=' + obj.q + '&appid=' + key

const pre = req => {
	Assert(shouldContainQueryParam)(req, errShouldContainQueryParam)
	Assert(shouldContainQParam)(req.query, errShouldContainQParam)
}

const fn = req => adaptor.get(query(req.query))

const proxy = req => {
	try {
		pre(req)
		return fn(req)
	} catch (err) {
		Logger.error(err.message)
		throw err
	}
}

const api = {
	pre: pre,
	fn: fn,
	proxy: proxy
}

module.exports = { api, query }
