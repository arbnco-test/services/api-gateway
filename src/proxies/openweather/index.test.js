const query = require('./index.js').query
const Proxy = require('./index.js').api

describe('Openweather Proxy - query', () => {
	it('query(params) with {q: testCity} should form correct url', () => {
		expect(query({ q: 'testCity' })).toBe('?q=testCity&appid=708c6b9f5408a4985e64bfbad5fe940d')
	})
})

describe('Openweather Proxy API - Preconditions', () => {
	it('precodition with params present should pass', () => {
		expect(Proxy.pre({ query: { q: 'testCity' } })).toBeUndefined()
	})
	it('precondition fails when params are not present in q', () => {
		expect(() => Proxy.pre({ query: { r: 'testCity' } })).toThrowError()
	})
})

describe('Openweather Proxy API - Proxy', () => {
	// it('precodition with params present should pass', () => {
	// 	expect(Proxy.proxy({ query: { q: 'testCity' } })).toBe('')
	// })
	it('precondition fails when params are not present in q', () => {
		expect(() => Proxy.proxy({ query: { r: 'testCity' } })).toThrowError()
	})
	it('precondition fails when query is not present in q', () => {
		expect(() => Proxy.proxy({ notaquery: { r: 'testCity' } })).toThrowError()
	})
})
