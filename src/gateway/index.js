const uuid = require('uuid/v4')
const Router = require('express').Router()

const Proxy = require('../proxies/openweather').api
const Publisher = require('../pubsub').api
const c = require('../config.json')

module.exports = ({ config, db }) => {
	//let api = Router()

	Router.get('/forecast', (req, res) => {
		Proxy.proxy(req).then(resp => {
			let forecast = resp.data
			let obj = {
				uuid: uuid(),
				timestamp: Date.now(), // could also use objectid.gettimestamp()
				forecast: forecast
			}
			Publisher.publish(obj)
			res.send(obj)
		})
	})

	return Router
}
